import Review from "../models/Review";


export const getReviewsByProductId = async (req: any, res: any) => {
  try {
    const review = await Review
      .find({ productId: req.params.id})
      .lean()
      .exec();

    res.status(200).json({ data: review });
  } catch (e) {
    console.error(e);
    res.status(400).end();
  }
};
export const createReview = async (req: any, res: any) => {
  try {
    const review = await Review.create({ ...req.body });
    res.status(201).json({ data: review });
  } catch (e) {
    console.error(e);
    res.status(400).end();
  }
};

