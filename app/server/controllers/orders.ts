import Order from "../models/Order";
import OrderedItem from "../models/OrderedItem";
import { sendInfo, sendInvoice } from "./../services/email/index";


export const order = async (req: any, res: any) => {
  try {
    const { order, orderedItems } = this.req.body;
    const orderAfterSave = await Order.create({ ...order });
    const items = orderedItems.array.forEach((item: any) => {
      return {
        ...item,
        orderId: orderAfterSave._id
      };
    });

    OrderedItem.insertMany(items);
    sendInfo(order);
    sendInvoice(order, orderedItems);
    res.status(200).end();
  } catch (e) {
    console.error(e);
    res.status(400).end();
  }
};
