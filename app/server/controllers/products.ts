import * as url from 'url';
import Product from "../models/Product";
import Review from "../models/Review";
import CategoryFlat from "../models/CategoryFlat";
import { DiscountedItemsIds, FavouriteItemsIds, ProductsRequiredCount } from "./config";


export const getProductById = async (req: any, res: any) => {
  try {
    const product = await Product
      .findOne({ _id: req.params.id})
      .populate("stock")
      .lean()
      .exec();

    res.status(200).json({ data: product });
  } catch (e) {
    console.error(e);
    res.status(400).end();
  }
};

export const getProductByName = async (req: any, res: any) => {
  try {
    const product = await Product
      .findOne({name: req.params.name})
      .populate("stock")
      .lean()
      .exec();

    res.status(200).json({ data: product});
  } catch (e) {
    console.error(e);
    res.status(400).end();
  }
};

export const getProductByCategory = async (req: any, res: any) => {
  const parsedUrl = url.parse(req.url, true);
  try {
    const categories = getListOfChildrenCategories(req.params.category);
    const products = await Product
      .find({ category: categories})
      .skip(Number(parsedUrl.query.from) || 0)
      .limit(12)
      .populate("stock")
      .lean()
      .exec();
    res.status(200).json({ data: products });
  } catch (e) {
    console.error(e);
    res.status(400).end();
  }
};

async function getListOfChildrenCategories(category: string) {
  const flatCategories: any = await CategoryFlat
      .find({})
      .lean()
      .exec();
  let targetCategory;

  for (const key in flatCategories[0]) {
    if (flatCategories[0].hasOwnProperty(key)) {
      if (category === flatCategories[0][key].name) {
        targetCategory = flatCategories[0][key];
        break;
      }
    }
  }

  if (!targetCategory) return [category];

  return targetCategory.children.reduce((acc: any[], category: any) => {
    return [...acc, category.name];
  }, [targetCategory.name]);
}

export const searchProducts = async (req: any, res: any) => {
  const parsedUrl = url.parse(req.url, true);
  try {
    const products = await Product
      .find({
        name: { $regex: req.params.term },
        category: { $regex: req.params.term },
        brand: { $regex: req.params.term }
      })
      .skip(Number(parsedUrl.query.from) || 0)
      .limit(12)
      .populate("stock")
      .lean()
      .exec();

    res.status(200).json({ data: products });
  } catch (e) {
    console.error(e);
    res.status(400).end();
  }
};

export const getDiscountedItems = async (req: any, res: any) => {
  try {
    const products = await Product
      .find({
        isDiscounted: true
      });

    res.status(200).json({data: products.splice(0, 4)});
  } catch (e) {
    console.error(e);
    res.status(400).end();
  }
};

export const getFavouritesItems = async (req: any, res: any) => {
  try {
    const reviews = await Review.find({
      ranking: 5
    });

    const productsIds = hasRequiredCount(reviews) ? mapReviewsToProductIds(reviews) : FavouriteItemsIds;
    const products = await Product.find({item: productsIds});
    res.status(200).json({data: products});
  } catch (e) {
    console.error(e);
    res.status(400).end();
  }
};

const mapReviewsToProductIds = (reviews: any) => {
  return reviews
    .sort((a: any, b: any) => a.ranking - b.ranking)
    .splice(0, 4)
    .map((review: any) => review.productId);
};

const hasRequiredCount = (list: any) => list.length >= ProductsRequiredCount;

export const setDiscountedItems = async (req: any, res: any) => {
  try {
    const products = await Product.find({item: DiscountedItemsIds});

    products.forEach(async (product: any) => {
      const discountedPrice =  Math.floor(product.price - (product.price * 0.10));
      await Product.findOneAndUpdate(
        {_id: product._id},
        { isDiscounted: true, discountedPrice }
      );
    });
    res.status(200).send("OK");
  } catch (e) {
    console.error(e);
    res.status(400).end();
  }
};
