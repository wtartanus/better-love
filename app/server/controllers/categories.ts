import CategoryTree from "../models/CategoryTree";
import CategoryFlat from "../models/CategoryFlat";
import Category from "../models/Category";

export const getCategories = async (req: any, res: any) => {
  try {
    const categories = await Category.find({});

    res.status(200).json({ data: categories });
  } catch (e) {
    console.error(e);
    res.status(400).end();
  }
};

export const getCategoryTree = async (req: any, res: any) => {
  try {
    const tree = await CategoryTree.find({});

    res.status(200).json({ data: { categoriesTree: tree[0] } });
  } catch (e) {
    console.error(e);
    res.status(400).end();
  }
};

export const getFlatCategories = async (req: any, res: any) => {
  try {
    const flatCategories = await CategoryFlat.find({});
    res.status(200).json({ data: { flatCategories: flatCategories[0] } });
  } catch (e) {
    console.error(e);
    res.status(400).end();
  }
};










