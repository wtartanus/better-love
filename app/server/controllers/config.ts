export const DiscountedItemsIds = [
  "21068",
  "21063",
  "21472",
  "24524"
];

export const FavouriteItemsIds = [
  "14534",
  "3822",
  "15012",
  "24541"
];

export const ProductsRequiredCount = 4;
