import * as mongoose from "mongoose";

const uri: string = "mongodb://127.0.0.1:27017/shop";

interface IOrderdItem extends mongoose.Document {
  orderId: string;
  productId: string;
  modelType: string;
  size: string;
  name: string;
  price: string;
}

const OrderedItemSchema = new mongoose.Schema({
  orderId: {
    type: mongoose.SchemaTypes.ObjectId,
    ref: "order",
  },
  productId: {
    type: mongoose.SchemaTypes.ObjectId,
    ref: "product",
  },
  modelType: String,
  size: String,
  name: String,
  price: String,
});

const OrderedItem = mongoose.model("orderedItem", OrderedItemSchema);
export default OrderedItem;
