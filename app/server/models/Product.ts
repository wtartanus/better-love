import * as mongoose from "mongoose";

const uri: string = "mongodb://127.0.0.1:27017/shop";

interface IProduct extends mongoose.Document {
  item: string;
  modelNum: string;
  name: string;
  weight: number;
  image: string;
  description: string;
  price: number;
  rrp: number;
  thumb: string;
  multi: string;
  multi1: string;
  multi3: string;
  bigMulti1: string;
  bigMulti2: string;
  bigMulti3: string;
  ean: string;
  ximage: string;
  ximage2: string;
  ximage3: string;
  ximage4: string;
  ximage5: string;
  length: string;
  lubeType: string;
  condomSafe: boolean;
  liquidVolum: string;
  numberOfPills: string;
  fastening: string;
  washing: string;
  insertable: string;
  diameter: string;
  harnessCompatible: boolean;
  oringCirc: string;
  originDiam: string;
  width: string;
  colour: string;
  flexability: string;
  controller: string;
  forWho: string;
  whatIsIt: string;
  for: string;
  motion: string;
  features: string;
  misc: string;
  waterproof: boolean;
  material: string;
  brand: string;
  style: string;
  power: string;
  size: string;
  opening: string;
  category: string;
  stock: any;
  isDiscounted: boolean;
  discountedPrice: number;
}

const ProductSchema = new mongoose.Schema({
  item: String,
  modelNum: String,
  name: String,
  weight: Number,
  image: String,
  description: String,
  price: Number,
  rrp: Number,
  thumb: String,
  multi: String,
  multi1: String,
  multi3: String,
  bigMulti1: String,
  bigMulti2: String,
  bigMulti3: String,
  ean: String,
  ximage: String,
  ximage2: String,
  ximage3: String,
  ximage4: String,
  ximage5: String,
  length: String,
  lubeType: String,
  condomSafe: Boolean,
  liquidVolum: String,
  numberOfPills: String,
  fastening: String,
  washing: String,
  insertable: String,
  diameter: String,
  harnessCompatible: Boolean,
  oringCirc: String,
  originDiam: String,
  width: String,
  colour: String,
  flexability: String,
  controller: String,
  forWho: String,
  whatIsIt: String,
  for: String,
  motion: String,
  features: String,
  misc: String,
  waterproof: Boolean,
  material: String,
  brand: String,
  style: String,
  power: String,
  size: String,
  opening: String,
  category: String,
  stock: {
    type: mongoose.SchemaTypes.ObjectId,
    ref: "stock",
  },
  isDiscounted: {
    type: Boolean,
    default: false
  },
  discountedPrice: {
    type: Number,
    default: undefined
  }
});

const Product = mongoose.model("product", ProductSchema);
export default Product;
