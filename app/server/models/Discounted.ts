import * as mongoose from "mongoose";
interface IDiscount extends mongoose.Document {
  item: string;
  discounted: boolean;
  date: string;
}

const DiscountSchema = new mongoose.Schema({
  item: String,
  discounted: Boolean,
  date: String
});

const Discount = mongoose.model("discount", DiscountSchema);
export default Discount;
