import * as mongoose from "mongoose";

const CategoryTreeSchema = new mongoose.Schema({}, {strict: false});

const CategoryTree = mongoose.model("categoryTree", CategoryTreeSchema);

export default CategoryTree;
