import * as mongoose from "mongoose";

const uri: string = "mongodb://127.0.0.1:27017/shop";

interface IStock extends mongoose.Document {
  item: string;
  inStock: boolean;
  size: string;
  name: string;
}

const StockSchema = new mongoose.Schema({
  item: String,
  inStock: Boolean,
  size: String,
  name: String
});

const Stock = mongoose.model("stock", StockSchema);
export default Stock;
