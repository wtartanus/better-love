import * as mongoose from "mongoose";

const uri: string = "mongodb://127.0.0.1:27017/shop";

interface IOrder extends mongoose.Document {
  referenceNumber: string;
  dateOrdered: Date;
  totalCost: number;
  totalPersonalCost: number;
  ordered: boolean;
  email: string;
  fullName: string;
  adres: string;
  city: string;
  postcode: string;
  deliveryType: string;
  deliveryPrice: string;
}

const OrderSchema = new mongoose.Schema({
  referenceNumber: String,
  dateOrdered: Date,
  totalCost: Number,
  totalPersonalCost: Number,
  ordered: Boolean,
  email: String,
  fullName: String,
  adres: String,
  city: String,
  postcode: String,
  deliveryType: String,
  deliveryPrice: String,
});

const Order = mongoose.model("order", OrderSchema);
export default Order;
