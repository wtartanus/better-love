import * as mongoose from "mongoose";

const uri: string = "mongodb://127.0.0.1:27017/shop";

interface IReview extends mongoose.Document {
  productId: string;
  text: string;
  ranking: number;
  name: string;
}

const ReviewSchema = new mongoose.Schema({
  productId: {
    type: mongoose.SchemaTypes.ObjectId,
    ref: "product",
  },
  text: String,
  ranking: Number,
  name: String,
});

const Review = mongoose.model("review", ReviewSchema);
export default Review;
