import * as mongoose from "mongoose";
interface ICategory extends mongoose.Document {
  categoryId: number;
  name: string;
  image: string;
  parentId: string;
}

const CategorySchema = new mongoose.Schema({
  categoryId: Number,
  name: String,
  image: String,
  parentId: String,
});

const Category = mongoose.model("category", CategorySchema);
export default Category;
