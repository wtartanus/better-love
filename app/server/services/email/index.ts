const nodemailer = require("nodemailer");

const { EMAIL_ACCOUNT, EMAIL_PASS, EMAIL_HOST } = process.env;
const transporter = nodemailer.createTransport({
  host: EMAIL_HOST,
  secureConnection: false,
  port: 587,
  tls: {
     ciphers: "SSLv3"
  },
  auth: {
      user: EMAIL_ACCOUNT,
      pass: EMAIL_PASS
  }
});

export const sendInfo = ({ totalCost }: any) => {
  const { EMAIL_SENDER, EMAIL_INFO_RECEIVER} = process.env;
  const mailOptions = {
    from: EMAIL_SENDER,
    to: EMAIL_INFO_RECEIVER,
    subject: "Order",
    html: "Order for: " + totalCost
  };

  transporter.sendMail(mailOptions, function(error: any, info: any) {
    if (error) {
        return console.log(error);
    }

    console.log("Message sent: " + info.response);
  });
};

export const sendInvoice = (order: any, orderedItems: any) => {
  const { EMAIL_SENDER } = process.env;

  const itemsList = createItemsList(orderedItems);
  const mailOptions = {
    from: EMAIL_SENDER,
    to: order.email,
    subject: "Order Confirmation",
    html: "<h3>Dear " + order.fullName + "</h3></br> Thanks for your order. Your order number is: <span style='color:green'>" +
    order.referenceNumber + "</span></br><h3>Delivery Address</h3><br/><p>" + order.adres + "</p></br>" +
    "<p>" + order.postcode + "</p></br><h4>Shipment info.</h4></br>" +
    "<p>" + order.deliveryType + "</p></br><h3>Items</h3></br><table><tr><th>Item</th><th>Quantity</th><th>Price</th></tr>" +
    itemsList + "<tr>Delivery</tr><tr><td colspan='2'>" + order.deliveryType + "</td><td>£" + order.deliveryPrice + "</td></tr>" +
    "<tr><td colspan='2'>Total Price</td><td>" + order.totalCost + "</td></tr></table>"
  };

  transporter.sendMail(mailOptions, function(error: any, info: any) {
    if (error) {
        return console.log(error);
    }

    console.log("Message sent: " + info.response);
  });
};

const createItemsList = (orderedItems: any) => {
  let itemsList = "";
  orderedItems.forEach((item: any) => {
    const listItem = "<tr><td>" + item.name + "</td><td>" + item.quantity + "</td><td>£" + item.price + "</td></tr>";
    itemsList += listItem;
  });

  return itemsList;
};
