const CronJob = require("cron").CronJob;
import { sendInfo } from "../email";

export const setDataUpdate = (clb: Function) => {
  new CronJob("00 00 04 * * *", function() {
    console.log("Starting cron job.");
    clb();
  }, undefined, true);
};



