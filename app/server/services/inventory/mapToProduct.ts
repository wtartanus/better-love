export const mapToProduct = (productItem: any, stockByItem: any) => {
  try {
    return {
      item: productItem._attributes ? productItem._attributes.ITEM : undefined,
      modelNum: productItem.MODEL ? productItem.MODEL._text : undefined,
      name: productItem.NAME ? productItem.NAME._text : undefined,
      weight: productItem.WEIGHT && productItem.WEIGHT._text ? Number(productItem.WEIGHT._text) : undefined,
      image: productItem.IMAGE ? productItem.IMAGE._text : undefined,
      description: productItem.DESCRIPTION ? productItem.DESCRIPTION._text : undefined,
      price: productItem.PRICE && productItem.PRICE._text ? Number(productItem.PRICE._text) : undefined,
      rrp: productItem.RRP && productItem.RRP._text ? Number(productItem.RRP._text) : undefined,
      thumb: productItem.THUMB ? productItem.THUMB._text : undefined,
      multi: productItem.MULTI ? productItem.MULTI._text : undefined,
      multi1: productItem.MULTI1 ? productItem.MULTI1._text : undefined,
      multi2: productItem.MULTI2 ? productItem.MULTI2._text : undefined,
      multi3: productItem.MULTI3 ? productItem.MULTI3._text : undefined,
      bigMulti1: productItem.BIGMULTI1 ? productItem.BIGMULTI1._text : undefined,
      bigMulti2: productItem.BIGMULTI2 ? productItem.BIGMULTI2._text : undefined,
      bigMulti3: productItem.BIGMULTI3 ? productItem.BIGMULTI3._text : undefined,
      ean: productItem.EAN ? productItem.EAN._text : undefined,
      ximage: productItem.XIMAGE ? productItem.XIMAGE._text : undefined,
      ximage2: productItem.XIMAGE2 ? productItem.XIMAGE2._text : undefined,
      ximage3: productItem.XIMAGE3 ? productItem.XIMAGE3._text : undefined,
      ximage4: productItem.XIMAGE4 ? productItem.XIMAGE4._text : undefined,
      ximage5: productItem.XIMAGE5 ? productItem.XIMAGE5._text : undefined,
      length: productItem.LENGTH ? productItem.LENGTH._text : undefined,
      lubeType: productItem.LUBTYPE ? productItem.LUBTYPE._text : undefined,
      condomSafe: productItem.CONDOMSAFE ? productItem.CONDOMSAFE["text"] : undefined,
      liquidVolum: productItem.LIQUIDVOLUMN ? productItem.LIQUIDVOLUMN._text : undefined,
      numberOfPills: productItem.NUMBEROFPILLS && productItem.NUMBEROFPILLS._text ? Number(productItem.NUMBEROFPILLS._text) : undefined,
      fastening: productItem.FASTENING ? productItem.FASTENING._text : undefined,
      washing: productItem.WASHING ? productItem.WASHING._text : undefined,
      insertable: productItem.INSERTABLE && productItem.INSERTABLE._text !== undefined ? Boolean(productItem.INSERTABLE._text) : undefined,
      diameter: productItem.DIAMETER ? productItem.DIAMETER._text : undefined,
      harnessCompatible: productItem.HARNESSCOMPATIBLE && productItem.HARNESSCOMPATIBLE._text != undefined ? Boolean(productItem.HARNESSCOMPATIBLE._text) : undefined,
      oringCirc: productItem.ORIGINCIRIC ? productItem.ORIGINCIRIC._text : undefined,
      originDiam: productItem.ORIGINDIAM ? productItem.ORIGINDIAM._text : undefined,
      width: productItem.WIDTH && productItem.WIDTH._text ? Number(productItem.WIDTH._text) : undefined,
      colour: productItem.COLOUR ? productItem.COLOUR._text : undefined,
      flexability: productItem.FLEXABILITY ? productItem.FLEXABILITY._text : undefined,
      controller: productItem.CONTROLLER ? productItem.CONTROLLER._text : undefined,
      forWho: productItem.FORWHO ? productItem.FORWHO._text : undefined,
      whatIsIt: productItem.WHATISIT ? productItem.WHATISIT._text : undefined,
      for: productItem.FOR ? productItem.FOR._text : undefined,
      motion: productItem.MOTION ? productItem.MOTION._text : undefined,
      features: productItem.FEATURES ? productItem.FEATURES._text : undefined,
      misc: productItem.MISC ? productItem.MISC._text : undefined,
      waterproof: productItem.WATERPROOF && productItem.WATERPROOF._text != undefined ? Boolean(productItem.WATERPROOF._text) : undefined,
      material: productItem.MATERIAL ? productItem.MATERIAL._text : undefined,
      brand: productItem.BRAND ? productItem.BRAND._text : undefined,
      style: productItem.STYLE ? productItem.STYLE._text : undefined,
      power: productItem.POWER ? productItem.POWER._text : undefined,
      size: productItem.SIZE ? productItem.SIZE._text : undefined,
      opening: productItem.OPENING ? productItem.OPENING._text : undefined,
      category: productItem.INCATNAME ? productItem.INCATNAME._text : undefined,
      stock: extractStock(productItem, stockByItem)
    };
  } catch (error) {
      console.log("mapping inventory", error);
  }
};

const extractStock = (productItem: any, stockByItem: any) => stockByItem[productItem.NAME._text];

