export const mapToCategory = (category: any) => {
  try {
    return {
      categoryId: category.categories_id._text,
      name: category.categories_name._text,
      image: category.categories_image._text,
      parentId: category.parent_id._text
    };
  } catch (error) {
    console.log("mapping category", error);
  }
};
