export const mapToStock = (stock: any) => {
  try {
    return {
      inStock: isItemInStock(stock),
      size: Array.isArray(stock.STOCK) ? JSON.stringify(stock.STOCK) : undefined,
      item: stock._attributes.ITEM,
      name: stock._attributes.NAME
    };
  } catch (error) {
    console.log("mapping stock", error);
  }
};

const isItemInStock = (stock: any) => {
  if (!Array.isArray(stock.STOCK)) {
    return stock.STOCK._text === "In Stock";
  } else {
    let inStock = false;
    stock.STOCK.forEach((item: any) => {
      inStock = !inStock ? item._text === "In Stock" : inStock;
    });
  }
};
