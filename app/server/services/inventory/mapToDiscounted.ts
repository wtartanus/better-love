export const mapToDiscounted = (discount: any) => {
  try {
    return {
      item: discount._attributes.ITEM,
      discounted: Boolean(discount.AVAILABLE._text),
      date: discount.DATE._text,
    };
  } catch (error) {
    console.log("mapping discounts", error);
  }
};
