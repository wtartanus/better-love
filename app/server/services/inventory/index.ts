import { Request, Response } from "express";
import axios from "axios";
import * as convert from "xml-js";

import Product from "../../models/Product";
import Stock from "../../models/Stock";
import Discount from "../../models/Discounted";
import Category from "../../models/Category";
import CategoryTree from "../../models/CategoryTree";
import CategoryFlat from "../../models/CategoryFlat";
import { mapToProduct } from "./mapToProduct";
import { mapToStock } from "./mapToStock";
import { mapToDiscounted } from "./mapToDiscounted";
import { mapToCategory } from "./mapToCategory";

export const updateInventory = async (req: Request, res: Response) => {
  console.time("categories");
  await updateCategories();
  console.timeEnd("categories");
  console.time("stock");
  await updateStock();
  console.timeEnd("stock");
  console.time("products");
  await updateProducts();
  console.timeEnd("products");
  console.time("discounted");
  await updateDiscounted();
  console.timeEnd("discounted");
  await createCategoryTree();
  await createCategoryFlat();
  if (res) res.send("OK");
};

const updateStock = async () => {
  try {
    const stock = await getJsonData("https://www.xtrader.co.uk/catalog/xml-feed/stockatt.xml");
    const extractedStock = stock.STOREITEMS.PRODUCT;

    const mappedStock = extractedStock.map((productStock: any) => mapToStock(productStock));
    updateItems(mappedStock, Stock, "item");
  } catch (error) {
    console.error("update stock, ", error);
  }
};

const updateProducts = async () => {
  try {
    const products = await getJsonData("https://www.xtrader.co.uk/catalog/xml_all.xml");
    const extractedProducts = products.STOREITEMS.CREATED.CATEGORY;

    const stockByName = await getStocksByName();

    const mappedProducts = new Array();
    extractedProducts.forEach((category: any) => {
      category["PRODUCT"].map((product: any) => mappedProducts.push(mapToProduct(product, stockByName)));
    });

    updateItems(mappedProducts, Product, "ean");
  } catch (error) {
    console.error("update products, ", error);
  }
};

const updateDiscounted = async () => {
  try {
    const discounted = await getJsonData("https://www.xtrader.co.uk/catalog/xml-feed/discontinued.xml");
    const extractedDiscounted = discounted.STOREITEMS.PRODUCT;

    const mappedDiscounted = extractedDiscounted.map((discounted: any) => mapToDiscounted(discounted));
    updateItems(mappedDiscounted, Discount, "item");
    markDiscountedProducts(mappedDiscounted);
  } catch (error) {
    console.error("update discounted, ", error);
  }
};

const markDiscountedProducts = async (discounted: any) => {
  discounted.forEach(async (discount: any) => {
    const { item } = discount;
    await Product
      .findOneAndUpdate(
        { item },
        { isisDiscounted: true}
      );
  });
};

const getStocksByName = async () => {
  const stocks = await Stock.find({});
  const stockByName: any = {};
  stocks.forEach((stock: any) => stockByName[stock.name] = stock);
  return stockByName;
};

const updateCategories = async () => {
  try {
    const category =  await getJsonData("https://www.xtrader.co.uk/catalog/xml_cat.xml");
    const extractedCategories = category.categories.CREATED.category;
    const mappedCategories = extractedCategories.map((category: any) => mapToCategory(category));
    const updatedCategories = aligneCategories(mappedCategories);
    updateItems(updatedCategories, Category, "name");
  } catch (error) {
    console.error("update categories, ", error);
  }
};

const aligneCategories = (categories: any) => {
  const forUpdate = ["Games", "Relaxation Zone", "Hen And Stag Nights", "Media", "Condoms"];
  const novelties = categories.find((category: any) => category.name == "Novelties");
  return categories.map((category: any) => {
    if (forUpdate.indexOf(category.name) > -1) {
      return {
        ...category,
        parentId: novelties.categoryId
      };
    } else {
      return category;
    }
  });
};

const getJsonData = async (url: string) => {
  const xml = await getXmlData(url);
  const json = convert.xml2json(xml, {compact: true, spaces: 4});
  return JSON.parse(json);
};

const getXmlData = async (url: string) => await axios.get(url).then(({ data }) => data);

const updateItems = (items: any, model: any, field: any) => {
  items.forEach((item: any) => {
    model.findOneAndUpdate(
      {[field]: item[field]},
      item,
      insertIfNotFound,
      (error: any, doc: any) => handleInsert(error, doc)
    );
  });
};

const insertIfNotFound = {
  upsert: true,
  new: true
};

const handleInsert = async (error: any, doc: any) => {
  if (doc) {
   await doc.save();
  } else if (error) {
    console.log(error);
  }
};

const createCategoryTree = async () => {
  await removeOldTree();
  const categories = await Category.find({});
  const categoriesTree = createCategoriesTree(categories);
  const tree = new CategoryTree(categoriesTree);
  tree.save();
};

const removeOldTree = async () => {
  const oldTree = await CategoryTree.find({});
  if (oldTree.length) oldTree[0].remove();
};

const createCategoriesTree = (categories: any) => {
  const result = { name: "Root", id: 0, children: [] as any[] };
  return populateChildren(result, categories);
};

const populateChildren = (node: any, categories: any) => {
  node.children = categories
  .filter((category: any) => category.parentId == node.id)
  .map((category: any) => {
    const { name, categoryId, image } = category;
    return {
      name,
      id: categoryId,
      children: [] as any[],
      image,
    };
  });

  if (node.children.length) {
    for (let i = 0; i < node.children.length; i++) {
      const child = node.children[i];
      populateChildren(child, categories);
    }
  }

  return node;
};

export const createCategoryFlat = async () => {
  await removeOldFlat();
  const categories = await Category.find({});
  const parentIds = getParentsIds(categories);

  const result = createFlatCategories(parentIds, categories);

  const flatCategories = populateWithChildren(result, categories);
  const flat = new CategoryFlat(flatCategories);
  await flat.save();
};

const removeOldFlat = async () => {
  const oldFlat = await CategoryFlat.find({});
  if (oldFlat.length) oldFlat[0].remove();
};

const getParentsIds = (categories: any) => {
  return categories
    .map((category: any) => category.parentId)
    .reduce((acc: any, id: any) => {
      if (acc.indexOf(id) < 0) acc.push(id);
      return acc;
    }, []);
};

const createFlatCategories = (parentIds: any, categories: any) => {
  const result = [] as any[];
  for (let i = 0; i < parentIds.length; i++) {
    let category: any;
    if (parentIds[i] == "0") {
      category = { name: "main menu", children: [] as any[]};
    } else {
      const found = categories
        .filter((cat: any) => cat.categoryId == parentIds[i])
        .map((cat: any) => {
          return {
            name: cat.name,
            parentId: cat.parentId,
            image: cat.image
          };
        })[0];
      const parent = categories.filter((a: any) => a.categoryId == found.parentId)[0];
      category = { name: found.name, parent: parent ? parent.toObject().name : "main menu", children: [] as any[] };
    }

    category.children = categories.filter((cat: any) => cat.parentId == parentIds[i]).map((a: any) => { return {name: a.name, parent: category.name, image: a.image, categoryId: a.categoryId, children: 0}; });
    result.push(category);
  }
  return result;
};

const populateWithChildren = (flatCategories: any, categories: any) => {
  for (let i = 0; i < flatCategories.length; i++) {
    const category = flatCategories[i];
    for (let j = 0; j < category.children.length; j++) {
      const { categoryId } = category.children[j];
      flatCategories[i].children[j].children = getLengthOfChildrenForCategory(categories, categoryId);
    }
  }
  return flatCategories;
};

const getLengthOfChildrenForCategory = (categories: any, categoryId: any) => {
  return categories.filter((category: any) => category.parentId == categoryId).length;
};
