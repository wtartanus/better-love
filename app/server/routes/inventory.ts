import { Router } from "express";
import * as InventoryService from '../services/inventory';

const router = Router();

router
  .route("api/updateDB")
  .post(InventoryService.updateInventory);

export default router;
