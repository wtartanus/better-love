import { Router } from "express";
import * as categoryController from "../controllers/categories";
import * as InventoryService from '../services/inventory';
import * as orderController from "../controllers/orders";
import * as productController from "../controllers/products";
import * as reviewController from "../controllers/rewievs";

const router = Router();

router
  .route("/categoryTree")
  .get(categoryController.getCategoryTree);

router
  .route("/categoryFlat")
  .get(categoryController.getFlatCategories);

router
  .route("/categories")
  .get(categoryController.getCategories);

router
  .route("/updateDB")
  .post(InventoryService.updateInventory);

router
  .route("/order")
  .post(orderController.order);

  router
  .route("/product/:name")
  .get(productController.getProductByName);

router
  .route("/category/:category")
  .get(productController.getProductByCategory);

router
  .route("/search/:term")
  .get(productController.searchProducts);

router
  .route("/discounted")
  .get(productController.getDiscountedItems);

router
  .route("/favourites")
  .get(productController.getFavouritesItems);

router
  .route("/setDiscountedItems")
  .post(productController.setDiscountedItems);

router
  .route("/review/:id")
  .get(reviewController.getReviewsByProductId);

router
  .route("/review/")
  .post(reviewController.createReview);

export default router;

