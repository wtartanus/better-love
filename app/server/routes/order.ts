import { Router } from "express";
import * as controller from "../controllers/orders";

const router = Router();

router
  .route("api/order")
  .post(controller.order);

export default router;
