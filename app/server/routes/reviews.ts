import { Router } from "express";
import * as controller from "../controllers/rewievs";

const router = Router();

router
  .route("api/review/:id")
  .get(controller.getReviewsByProductId);

router
  .route("api/review/")
  .post(controller.createReview);

export default router;
