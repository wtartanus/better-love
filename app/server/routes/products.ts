import { Router } from "express";
import * as controller from "../controllers/products";

const router = Router();

// router
//   .route("/product/:id")
//   .get(controller.getProductById);

router
  .route("/api/product/:name")
  .get(controller.getProductByName);

router
  .route("/api/category/:category")
  .get(controller.getProductByCategory);

router
  .route("/api/search/:term")
  .get(controller.searchProducts);

router
  .route("/api/discounted")
  .get(controller.getDiscountedItems);

router
  .route("/api/favourites")
  .get(controller.getFavouritesItems);

router
  .route("/api/setDiscountedItems")
  .post(controller.setDiscountedItems);

export default router;
