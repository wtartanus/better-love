import { Router } from "express";
import * as controller from "../controllers/categories";

const router = Router();

router
  .route("api/categoryTree")
  .get(controller.getCategoryTree);

router
  .route("api/categoryFlat")
  .get(controller.getFlatCategories);

router
  .route("api/categories")
  .get(controller.getCategories);

export default router;
