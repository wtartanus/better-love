import {
  NgModule,
  PLATFORM_ID,
  APP_ID, Inject
} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CarouselModule } from 'primeng/carousel';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { isPlatformBrowser } from '@angular/common';
import { LazyLoadImageModule, intersectionObserverPreset } from 'ng-lazyload-image';

import { AppComponent } from './app.component';
import { BasketPageComponent } from './Components/BasketPage';
import { CategoryComponent } from './Components/Category';
import { CheckoutComponent } from './Components/Checkout';
import { ContactPage } from './Components/ContactPage';
import { Delivery } from './Components/Delivery';
import { HomeComponent } from './Components/Home';
import { Privacy } from './Components/Privacy';
import { ProductsComponent } from './Components/Pages/Products';
import { Returns } from './Components/Returns';
import { ProductPage } from './Components/Pages/Product';
import { SplashComponent } from './Components/SplashPage';
import { TermsAndConditions } from './Components/TermsAndConditions';

import { AgeVerify } from './Partials/AgeVerify';
import { BasketComponent } from './Partials/Basket';
import { BrandsComponent } from './Partials/Brands';
import { BreadcrumbComponent } from './Partials/Breadcrumbs';
import { CarouselComponent } from './Partials/Carousel';
import { CookiePolicyPopUp } from './Partials/CookiePolicyPopUp';
import { CollectionsComponent } from './Partials/collections';
import { FooterComponent } from './Partials/Footer';
import { FreeDevlivery } from './Partials/FreeDelivery/index';
import { HeaderComponent } from './Partials/Header';
import { HomePageProducts } from './Partials/HomePageProducts/index';
import { InfoIcons } from './Partials/InfoIcons/index';
import { MenuBigComponent } from './Partials/menu-big';
import { MenuComponent } from './Partials/menu';
import { MenuFooterComponent } from './Partials/menu-footer';
import { PaginationComponent } from './Partials/pagination';
import { ProductPhotos } from './Components/Pages/Product/productPhotos';
import { ProductInfo } from './Components/Pages/Product/productInfo';
import { ProductSpecifications } from './Components/Pages/Product/specifications';
import { ProductReviews } from './Components/Pages/Product/reviews';
import { SearchComponent } from './Partials/Search';
import { SearchOverlayComponent } from './Partials/search-overlay';
import { SizeChart } from './Components/Pages/Product/sizeChart';
import { ToastComponent } from './Partials/Toast/index';
import { InlineLoadingComponent } from './Components/Shared/InlineLoading';

import { LoadingComponent } from './Components/Shared/Loading';
import { SortingComponent } from './Components/Pages/Products/Sorting'
import { ProductsListingComponent } from './Components/Pages/Products/ProductsListing';
import { ProductListingComponent } from './Components/Pages/Products/ProductListing';

import { 
  BasketService, 
  BreadcrumbsService,
  CategoriesService,
  MessageService,
  OrdersService,
  ProductsService,
  Requestor,
  UtilitiesService,
  ReviewsService
} from './Services';

import { CarouselItemDirective } from './Directives/carousel-item.directive';
import { CarouselItemElementDirective } from './Directives/carousel-item-element.directive';



@NgModule({
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'better-love' }),
    CarouselModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    InfiniteScrollModule,
    LazyLoadImageModule.forRoot({
      preset: intersectionObserverPreset
    })
  ],
  declarations: [
    AppComponent,
    BasketPageComponent,
    CategoryComponent,
    CheckoutComponent,
    ContactPage,
    Delivery,
    HomeComponent,
    Privacy,
    ProductsComponent,
    Returns,
    ProductPage,
    SplashComponent,
    TermsAndConditions,

    AgeVerify,
    BasketComponent,
    BrandsComponent,
    BreadcrumbComponent,
    CarouselComponent,
    CarouselItemDirective,
    CarouselItemElementDirective,
    CookiePolicyPopUp,
    CollectionsComponent,
    FooterComponent,
    FreeDevlivery,
    HeaderComponent,
    HomePageProducts,
    InfoIcons,
    MenuBigComponent,
    MenuComponent,
    MenuFooterComponent,
    PaginationComponent,
    ProductPhotos,
    ProductInfo,
    ProductSpecifications,
    ProductReviews,
    SearchComponent,
    SearchOverlayComponent,
    SizeChart,
    ToastComponent,
    LoadingComponent,
    SortingComponent,
    ProductsListingComponent,
    ProductListingComponent,
    InlineLoadingComponent
  ],
  providers: [
    BasketService, 
    BreadcrumbsService,
    CategoriesService, 
    MessageService, 
    OrdersService,
    ProductsService, 
    Requestor,
    UtilitiesService, 
    ReviewsService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string) {
    const platform = isPlatformBrowser(platformId) ?
      'in the browser' : 'on the server';
    console.log(`Running ${platform} with appId=${appId}`);
  }
}
