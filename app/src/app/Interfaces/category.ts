export interface ICategory {
  _id: string;
  categoryId: number;
  name: string;
  image: string;
  parentId: string;
}