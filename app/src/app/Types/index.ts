export enum TOAST_TYPES {
  success
}

export enum HOME_PAGE_PRODUCTS_DISPLAY_TYPES {
  favourites,
  discounted
}
