import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SplashComponent } from './Components/SplashPage';
import { BasketPageComponent } from './Components/BasketPage';
import { ProductsComponent } from './Components/Pages/Products';
import { CheckoutComponent } from './Components/Checkout';
import { ProductPage } from './Components/Pages/Product';
import { TermsAndConditions } from './Components/TermsAndConditions';
import { Returns } from './Components/Returns';
import { Privacy } from './Components/Privacy';
import { Delivery } from './Components/Delivery';
import { HomeComponent } from './Components/Home';
import { ContactPage } from './Components/ContactPage';

const routes: Routes = [
  { path: '', component: HomeComponent, children: 
    [
      { path: '', component: SplashComponent },
      { path: 'basket', component: BasketPageComponent, data: { breadcrumb: 'Basket'} },
      { path: 'checkout', component: CheckoutComponent, data: { breadcrumb: 'Checkout'} },
      { path: 'contact-us', component: ContactPage },
      { path: 'policy/termsandconditions', component: TermsAndConditions, data: { breadcrumb: 'Terms And Conditions'} },
      { path: 'policy/privacy', component: Privacy, data: { breadcrumb: 'Privacy Policy'} },
      { path: 'policy/returns', component: Returns, data: { breadcrumb: 'Returns Policy'} },
      { path: 'policy/delivery', component: Delivery, data: { breadcrumb: 'Delivery Policy'} },
      { path: 'search/:searchQuery', component: ProductsComponent, data: { breadcrumbFromParam: 'true', paramKey: 'searchQuery' } },
      { path: 'product/:name', component: ProductPage },
      { path: 'category/:category', component: ProductsComponent, data: { breadcrumbFromParam: 'true', paramKey: 'category' }}
    ] 
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

