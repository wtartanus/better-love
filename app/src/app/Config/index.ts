export const TOAST_TIMEOUT = 1500;
export const TOAST_DISPLAY_MESSAGE = 'toast-show';

export const BREADCRUMBS_UPDATED = 'breadcrumbs-updated';
