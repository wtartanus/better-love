import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { MessageService } from '../../Services';

const DISPLAY_LENGTH = 5;

@Component({
  selector: 'app-pagination',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class PaginationComponent implements OnInit {
  @Input() pagesCount: Array<number>;
  @Output() pageChange = new EventEmitter();
  displayPages: Array<number> = new Array();
  selectedPage: number;
  private subscription: Subscription;

  constructor (private messageService: MessageService) {
    this.subscription = this.messageService.getMessage().subscribe(message => this.processMessage(message));
  }

  processMessage(message: any): void {
    if (message.text === 'refresh-pagination') this.refresh(message.body.pages);
  }

  refresh(pages: Array<number>) {
    this.selectedPage = 0;
    this.displayPages = this.getDisplayPages(pages, this.selectedPage);
  }

  ngOnInit() {
    this.displayPages = this.getDisplayPages(this.pagesCount, this.selectedPage);
    this.selectedPage = 0;
  }

  getDisplayPages(pages: Array<number>, selectedPage: number): Array<number> {
    if (!this.isPageSelected(selectedPage)) {
      if (this.isWithinDisplayLength(pages)) {
        return [...pages];
      } else {
        return pages.slice(0, DISPLAY_LENGTH);
      }
    } else {
      if (this.isWithinDisplayLength(pages)) return [...pages];
      if (this.isOnTheStart(pages, selectedPage)) return pages.slice(0, DISPLAY_LENGTH);
      if (this.isOnTheEnd(pages, selectedPage)) return pages.slice((pages.length - DISPLAY_LENGTH), pages.length);
      return this.getMiddleDisplayList(pages, selectedPage);
    }
  }

  isPageSelected(selectedPage?: number): boolean {
    return !isNaN(selectedPage);
  }

  isWithinDisplayLength(list: Array<number>): boolean {
    return list.length <= DISPLAY_LENGTH;
  }

  isOnTheStart(list: Array<number>, selectedPage: number): boolean {
     const index = list.indexOf(selectedPage);
     return index <= 2;
  }

  isOnTheEnd(list: Array<number>, selectedPage: number): boolean {
    const index = list.indexOf(selectedPage);
    const range = list.length - 4;
    return index >= range && index <= list.length - 1;
  }

  getMiddleDisplayList(pages: Array<number>, selectedPage: number): Array<number> {
    const index = pages.indexOf(selectedPage);
    return pages.slice(index - 2, index + 3);
  }

  goToStartEnd(goToStart?: boolean): void {
    if (goToStart) {
      this.changePage(0);
    } else {
      this.changePage(this.pagesCount[this.pagesCount.length - 1]);
    }
  }

  moveByOnePage(goLeft?: boolean): void {
    if (goLeft) {
      if (this.selectedPage) this.changePage(this.selectedPage - 1);
    } else {
      if (this.selectedPage !== this.pagesCount.length - 1) this.changePage(this.selectedPage + 1);
    }
  }

  changePage(pageIndex: number): void {
    this.selectedPage = pageIndex;
    this.displayPages = this.getDisplayPages(this.pagesCount, pageIndex);
    this.pageChange.emit(pageIndex);
  }
}
