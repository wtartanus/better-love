import { Component } from '@angular/core';

const ITEMS = [
  {
    title: 'BODIES AND PLAYSUITS',
    imgSm: 'https://s3.eu-west-2.amazonaws.com/petitcheri/caru/bodiesAndPlaysuits-mobile.jpg',
    imgM: 'https://s3.eu-west-2.amazonaws.com/petitcheri/caru/bodies-medium.jpg',
    imgL: 'https://s3.eu-west-2.amazonaws.com/petitcheri/caru/bodies.jpg',
    url: 'category/Bodies and Playsuits'
  },
  {
    title: 'VIBRATORS',
    imgSm: 'https://s3.eu-west-2.amazonaws.com/petitcheri/caru/vibrators-mobile.jpg',
    imgM: 'https://s3.eu-west-2.amazonaws.com/petitcheri/caru/vibrators-medium.jpg',
    imgL: 'https://s3.eu-west-2.amazonaws.com/petitcheri/caru/vibrators.jpg',
    url: 'category/G-Spot Vibrators'
  },
  {
    title: 'BASQUES AND CORSETS',
    imgSm: 'https://s3.eu-west-2.amazonaws.com/petitcheri/caru/corset-mobile.jpg',
    imgM: 'https://s3.eu-west-2.amazonaws.com/petitcheri/caru/corset-medium.jpg',
    imgL: 'https://s3.eu-west-2.amazonaws.com/petitcheri/caru/corset.jpg',
    url: 'category/Basques and Corsets'
  },
  {
    title: 'MALE CHASTITY',
    imgSm: 'https://s3.eu-west-2.amazonaws.com/petitcheri/caru/cage-mobile.jpg',
    imgM: 'https://s3.eu-west-2.amazonaws.com/petitcheri/caru/cage-medium.jpg',
    imgL: 'https://s3.eu-west-2.amazonaws.com/petitcheri/caru/cage.jpg',
    url: 'category/Male Chastity'
  }
]

@Component({
  selector: 'app-collections',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class CollectionsComponent {
  items = ITEMS;
}
