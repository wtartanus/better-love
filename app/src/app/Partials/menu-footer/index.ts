import { BasketComponent } from '../Basket/index';
import { Component, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'menu-footer',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css'],
  entryComponents: [BasketComponent]
})
export class MenuFooterComponent {
  @Input() displaySearch: boolean;
  @Output() showSearchOveralay = new EventEmitter();

  constructor() { }

  showSearch(): void {
    this.displaySearch = true;
    this.showSearchOveralay.emit('open');
  }
}
