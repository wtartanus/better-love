import { Component } from "@angular/core";
import { Subscription } from 'rxjs/Subscription';
import { MessageService } from '../../Services';
import { BREADCRUMBS_UPDATED } from '../../Config';


@Component({
  selector: 'breadcrumbs',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class BreadcrumbComponent {
  crumbs: any[] = new Array();
  private subscription: Subscription;

  constructor(private messageService: MessageService) {
    this.subscription = this.messageService.getMessage().subscribe(message => this.processMessage(message));
  }

  processMessage(message: any): void {
    if (message.text === BREADCRUMBS_UPDATED) {
      this.crumbs = [...message.body];
    }
  }

  resetBreadcrumbs() {
    this.crumbs.length = 0;
  }
}