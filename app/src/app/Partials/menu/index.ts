import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { MessageService } from '../../Services';
import Data from './menu.js'

const MainMenu = { name: 'main menu' };

@Component({
  selector: 'app-menu',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class MenuComponent implements OnInit {
  showNavigation: boolean;
  selectedCategory: any;
  tree: any;
  private subscription: Subscription;

  constructor (private messageService: MessageService) {
    this.subscription = this.messageService.getMessage().subscribe(message => this.processMessage(message));
  }

  async ngOnInit() {
    this.selectedCategory = MainMenu;
    const { flatCategories: result } = Data.data;
    this.tree = Object.values(result);
  }

  processMessage(message: any): void {
    if (message.text === 'close-nav') this.hideNav();
  }

  hideNav(): void {
    this.showNavigation = false;
    this.selectedCategory = MainMenu;
  }

  goBack() {
    const parent = this.getParentCategoryForCurrentlySelected();
    this.selectCategory(parent);
  }

  getParentCategoryForCurrentlySelected() {
    return this.tree.filter((category: any) => category.name == this.selectedCategory.parent)[0];
  }

  selectCategory(category: any): void {
    this.selectedCategory = category;
  }

  isSelected(category: string): boolean {
    return this.selectedCategory.name === category;
  }
}
