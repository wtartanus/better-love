import { SearchComponent } from './../Search/index';
import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'search-overlay',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css'],
  entryComponents: [SearchComponent]
})
export class SearchOverlayComponent {
  @Output() closeOverlay = new EventEmitter();
  constructor() { }

  close(): void {
    this.closeOverlay.emit();
  }
}
