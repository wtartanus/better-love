import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cookie-policy-popup',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class CookiePolicyPopUp implements OnInit {
	showPolicy: boolean = true;

	ngOnInit() {
    if (typeof localStorage !== undefined) {
      this.showPolicy = !Boolean(Number(localStorage.getItem('isPolicyAccepted')));
    }
  }

	closePopUp() {
		if (typeof localStorage !== undefined) {
			localStorage.setItem("isPolicyAccepted", '1');
			this.showPolicy = false;
		}
	}
}
