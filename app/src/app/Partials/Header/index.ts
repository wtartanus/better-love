import { Component, AfterViewInit, Output, EventEmitter  } from '@angular/core';

import { MenuComponent } from '../../Partials/menu';
import { BasketComponent } from '../Basket';
import { SearchComponent } from '../../Partials/Search/index';

@Component({
  selector: 'ng-header',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css'],
  entryComponents: [MenuComponent, BasketComponent, SearchComponent]
})
export class HeaderComponent implements AfterViewInit {
  @Output() showSearchOveralay = new EventEmitter();

  ngAfterViewInit() {
    this.setSpacerHeight();
    this.registerScrollEvent();
  }

  setSpacerHeight() {
    try {
      this.assertThatWindowExist();
      const spacer = document.getElementById('spacer');
      const header = document.getElementsByTagName('header')[0];
      spacer.style.height = this.getSpacerHeight(header.offsetHeight, header.offsetWidth);
    } catch (e) {
      // Fell throught
    }
  }

  getSpacerHeight(headerHeight, headerWidth) {
    return headerHeight + (headerWidth > 1080 ? 30 : 0) + 'px';
  }

  registerScrollEvent() {
    try {
      this.assertThatWindowExist();
      window.addEventListener('scroll', this.toggleLogo);
    } catch (e) {}
  }

  toggleLogo() {
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
      document.querySelector('.header__logo').classList.add('header__logo--hide');
    } else {
      document.querySelector('.header__logo').classList.remove('header__logo--hide');
    }
  }

  assertThatWindowExist() {
    if (typeof window === 'undefined') throw new Error();
  }
}
