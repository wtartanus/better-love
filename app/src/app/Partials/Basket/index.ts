import { Component, OnInit, Input } from "@angular/core";
import { Subscription } from 'rxjs/Subscription';
import { BasketService } from '../../Services';
import { MessageService } from '../../Services/message';

@Component({
  selector: 'basket-component',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class BasketComponent implements OnInit {
  private subscription: Subscription;
  basketValues: any = { itemsCount: 0, totalCost: 0 };
  @Input() menuFooterBasket: boolean;

  constructor (private basket: BasketService, private messageService: MessageService) {}

  ngOnInit() {
    this.subscription = this.messageService.getMessage().subscribe(message => this.processMessage(message));
    this.updateBasket();
  }

  processMessage(message: any): void {
    if (this.isBasketUpdate(message.text)) {
      this.updateBasket();
    }
  }

  isBasketUpdate(message: string) {
    return message === 'basket-update';
  }

  updateBasket() {
    const { itemsCount, totalCost } = this.basket;
    this.basketValues.itemsCount = itemsCount;
    this.basketValues.totalCost = totalCost;
  }
}
