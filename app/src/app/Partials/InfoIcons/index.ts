import { Component } from '@angular/core';

@Component({
  selector: 'info-icons',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class InfoIcons {}
