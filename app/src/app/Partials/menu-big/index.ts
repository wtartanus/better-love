import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { MessageService } from '../../Services';
import  Data from './menu.js';

@Component({
  selector: 'app-menu-big',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class MenuBigComponent implements OnInit {
  selectedCategory?: any;
  categories: any;
  selected: any[] = [];
  mainMenu: any;

  private subscription: Subscription;

  constructor (private messageService: MessageService) {
    this.subscription = this.messageService.getMessage().subscribe(message => this.processMessage(message));
  }

  async ngOnInit() {
    const {categoriesTree: categoriesRoot} = Data.data;
    categoriesRoot.children = categoriesRoot.children.filter((category: any) => category.name !== 'Media');
    this.categories = categoriesRoot;
  }
  processMessage(message: any): void {
    if (message.text === 'close-nav') this.hideNav();
  }

  hideNav() {
    this.selectedCategory = null;
    this.selected.length = 0;
  }

  selectCategory(category: any) {
    if (!this.selected.length) {
      this.selected.push(category);
    } else if (this.isCategorySelected(category)) {
      const cats = this.getAllSelectableChildrenNames(category, []);
      this.selected = this.selected.filter((item: any) => cats.indexOf(item.name) < 0);
    } else if (this.isSameRoot(category, this.selected[0])) {
      this.handleSameRootSelection(category);
    } else {
      this.selected.length = 0;
      this.selected.push(category);
    }
  }

  getAllSelectableChildrenNames(category: any, accumulator: Array<any>): Array<string> {
    if (category.children.length) {
      accumulator.push(category.name);
      for (let i = 0; i < category.children.length; i++) {
        this.getAllSelectableChildrenNames(category.children[i], accumulator);
      }
    }
    return accumulator;
  }

  isSameRoot(category: any, root: any, found: boolean = false): boolean {
    if (root.name === category.name) {
      found = true;
    }

    if (!found && root.children.length) {
      for (let i = 0; i < root.children.length; i++) {
        found = !found ? this.isSameRoot(category, root.children[i], found) : found;
      }
    }
    return found;
  }

  handleSameRootSelection(category: any) {
    if (this.selected[this.selected.length - 1].children.some((item: any) => item.name === category.name)) {
      this.selected.push(category);
    } else {
      for (let i = 0; i < this.selected.length; i++) {
        if (this.selected[i].children.some((item: any) => item.name === category.name)) {
          this.selected.splice(i + 1, this.selected.length - i + 2, category);
        }
      }
    }
  }

  isCategorySelected(category: any): boolean {
    return this.selected.some((item) => item.name === category.name);
  }
}
