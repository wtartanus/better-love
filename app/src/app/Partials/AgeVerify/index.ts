import { Component, Inject, OnInit } from "@angular/core";
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'age-verify',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class AgeVerify implements OnInit {
  isMouseOverUnderButton: boolean = false;
  showVerification: boolean = true;

  constructor(@Inject(DOCUMENT) private document: any) { }

  ngOnInit() {
    if (typeof localStorage !== undefined) {
      this.showVerification = !Boolean(Number(localStorage.getItem('isAgeVeryfied')));
    }
  }
  proceedToPage() {
    if (typeof localStorage !== undefined) {
      localStorage.setItem("isAgeVeryfied", '1');
      this.showVerification = false;
    }
  }

  leavePage() {
    this.document.location.href = 'http://www.google.com';
  }
}
