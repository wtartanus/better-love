import { Component, OnInit, OnDestroy, ViewChild, ElementRef} from "@angular/core";
import { CategoriesService } from '../../Services';

@Component({
  selector: 'app-brands',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class BrandsComponent implements OnInit, OnDestroy {
  @ViewChild('brandsSlider') brandsSlider: ElementRef;
  @ViewChild('sliderContainer') sliderContainer: ElementRef;
  brands: any[] = [];
  scrollRange: any = { right: 0, left: 0 };
  maxRight: number = 0;
  maxLeft: number = 0;
  initialPositions: any = { offsetX: 0, clientX: 0 };
  canProceedWithScroll: boolean = false;
  isLeftAutoScrollDirection: boolean = true;
  autoScrollInterval: any;

  constructor (private categoriesService: CategoriesService) {}

  async ngOnInit() {
    await this.setBrands();
    this.defineScrollRange();
    this.setUpAutoScroll();
  }

  async setBrands() {
    const categories = await this.categoriesService.getFlatCategories();
    this.brands = this.getBrands(categories);
  }

  getBrands(categories) {
    let result = [];
    for (let key in categories) {
      if (categories.hasOwnProperty(key)) {
        if (categories[key].name == 'Branded Toys') {
          result = [...categories[key].children];
          break;
        }
      }
    }
    return result;
  }

  defineScrollRange() {
    this.scrollRange.left = this.getScrollRange();
  }

  getScrollRange() {
    return this.brandsSlider.nativeElement.clientWidth - (this.brands.length * 100 + this.brands.length * 20);
  }

  setUpAutoScroll() {
    try {
      this.assertWindowIsDefined();
      this.autoScrollInterval = setInterval(this.autoScroll, 4000);
    } catch {}
  }

  ngOnDestroy() {
    this.clearAutoScrollInterval();
  }

  clearAutoScrollInterval() {
    try {
      this.assertWindowIsDefined();
      clearInterval(this.autoScrollInterval);
    } catch {}
  }


  assertWindowIsDefined() {
    if (typeof window === 'undefined') throw new Error();
  }

  autoScroll = () => {
    const currentPosition = this.getCurrentPosition();
    const newPosition = this.isLeftAutoScrollDirection ? currentPosition - 300 : currentPosition + 300;
    this.sliderContainer.nativeElement.style.left = this.aligneNewPosition(newPosition, true) + 'px';
  }

  getCurrentPosition() {   
    return parseInt(this.sliderContainer.nativeElement.style.left) || 0;
  }

  setInitialPositions(event) {
    this.initialPositions = this.getXPositions(event);
    this.canProceedWithScroll = true;
  }

  getXPositions(event) {
    const clientX = event.touches ? event.touches[0].clientX : event.clientX;
    const offsetX = parseInt(this.sliderContainer.nativeElement.style.left) || 0;
    return { offsetX, clientX };
  }

  scrollBrands(event) {
    try {
      this.assertScrollIsPossible();
      event.presist = true;
      const clientX = event.touches ? event.touches[0].clientX : event.clientX;
      const newPosition = this.initialPositions.offsetX + (clientX - this.initialPositions.clientX);
      this.sliderContainer.nativeElement.style.left = this.aligneNewPosition(newPosition) + 'px';
    } catch {}
  }

  assertScrollIsPossible() {
    if (!this.canProceedWithScroll) throw new Error();
  }

  aligneNewPosition(newPosition: number, modifyScrollDirection: boolean = false) {
    if (newPosition > this.scrollRange.right) {
      newPosition = this.scrollRange.right;
      this.isLeftAutoScrollDirection = modifyScrollDirection ? true : this.isLeftAutoScrollDirection;
    }
    if (newPosition < this.scrollRange.left) {
      newPosition = this.scrollRange.left;
      this.isLeftAutoScrollDirection = modifyScrollDirection ? false : this.isLeftAutoScrollDirection;
    }

    return newPosition;
  }

  resetIsScrollable() {
    this.canProceedWithScroll = false;
  }
}
