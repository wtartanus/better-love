import { Component } from '@angular/core';

@Component({
  selector: 'footerComponent',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class FooterComponent {}
