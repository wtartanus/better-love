import { Component, AfterViewInit, Input } from '@angular/core';
import { ProductsService } from '../../Services';
import { HOME_PAGE_PRODUCTS_DISPLAY_TYPES } from '../../Types';

@Component({
  selector: 'home-page-products',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css'],
})
export class HomePageProducts implements AfterViewInit {
  @Input() title: string;
  @Input() type: HOME_PAGE_PRODUCTS_DISPLAY_TYPES;
  products: any;

  constructor(public productsService: ProductsService) {};
  async ngAfterViewInit() {
    if (this.type === HOME_PAGE_PRODUCTS_DISPLAY_TYPES.favourites) {
      const products = await this.productsService.getFavourites();
      this.products = [...products.data];
    } else if (this.type === HOME_PAGE_PRODUCTS_DISPLAY_TYPES.discounted) {
      const products = await this.productsService.getDiscounted();
      this.products = [...products.data];
    }
  }
}
