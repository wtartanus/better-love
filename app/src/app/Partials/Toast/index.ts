import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { MessageService } from '../../Services';
import { TOAST_TYPES } from './../../Types/index';
import { TOAST_TIMEOUT, TOAST_DISPLAY_MESSAGE } from './../../Config/index';

@Component({
  selector: 'toast-component',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class ToastComponent implements OnInit {
  title: string;
  message: string;
  type: TOAST_TYPES;
  showToast = false;
  toastTypes = TOAST_TYPES;
  private subscription: Subscription;

  constructor (private messageService: MessageService) {}

  ngOnInit() {
    this.subscription = this.messageService.getMessage()
      .subscribe(message => this.processMessage(message));
  }

  processMessage(message: any): void {
    if (this.isToastShow(message.text)) {
      this.initToast(message);
    }
  }

  private isToastShow(message: string): boolean {
    return message === TOAST_DISPLAY_MESSAGE;
  }

  private initToast(body: any) {
    const { type, title, message } = body.body;
    this.title = title;
    this.message = message;
    this.type = type;

    this.displayToast();
    this.setToastCloseTimeout();
  }

  private displayToast() {
    this.showToast = true;
  }

  setToastCloseTimeout() {
    setTimeout(() => {
      this.closeToast();
    }, TOAST_TIMEOUT);
  }

  closeToast() {
    this.showToast = false;
  }
}
