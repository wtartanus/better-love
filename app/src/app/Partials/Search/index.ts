import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Router } from '@angular/router';

@Component({
  selector: 'search-component',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class SearchComponent {
  @Input() isOverlay: boolean;
  @Output() closeSearch = new EventEmitter();
  searchQuery = '';

  constructor(private router: Router) {}

  search(): void {
    if (this.isOverlay) this.closeSearch.emit();
    this.router.navigate(['/search', this.searchQuery]);
  }
}
