import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { BasketService, UtilitiesService } from '../../Services';

@Component({
  selector: 'basket',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class BasketPageComponent implements OnInit {
  basketItems: any;
  itemsCount: number;
  totalCost: number;

  constructor (
    private basket: BasketService,
    private router: Router,
    private utilities: UtilitiesService
  ) {};

  ngOnInit() {
    this.utilities.scrollToTop();
    const { itemsCount, totalCost, basketItems } = this.basket;

    this.basketItems = basketItems;
    this.itemsCount = itemsCount;
    this.totalCost = totalCost;
  }

  removeItem(product) {
    this.basket.removeFromBasket(product);
  }
}
