import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { UtilitiesService, ProductsService, BreadcrumbsService } from '../../../Services';

@Component({
  selector: 'product-page',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class ProductPage implements OnInit {
  product: any;
  loading = true;

  panelsDisplay: any = {
    specifications: true,
    reviews: false,
    sizeChart: false
  }

  constructor(
    private productsService: ProductsService,
    private route: ActivatedRoute,
    private router: Router,
    private utitlities: UtilitiesService,
    private breadcrumbs: BreadcrumbsService
  ) {};

  ngOnInit() {
    this.utitlities.scrollToTop();
    this.getProduct();
  }

  getProduct() {
    this.route.params.subscribe(async () => {
      this.loading = true;
      const productName = this.route.snapshot.paramMap.get('name');
      const result = await this.productsService.getProductByName(productName);
      this.product = {...result.data};
      this.breadcrumbs.setCurrentPosition(this.breadcrumbs.types.PRODUCT, productName, this.product.category);
      this.loading = false;
    });
  }
}
