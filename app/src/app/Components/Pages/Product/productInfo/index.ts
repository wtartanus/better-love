import { Component, Input, OnInit } from '@angular/core';
import { BasketService, ProductsService } from '../../../../Services';


@Component({
  selector: 'product-info',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class ProductInfo implements OnInit {
  @Input() product: any;
  quantity: number = 1;
  size: any;

  averageRanking: number = 5;

  constructor(public basket: BasketService, private productsService: ProductsService) {}

  ngOnInit() {
    this.mapProductSize();
  }

  private mapProductSize() {
    try {
      this.assertStockExists();
      const { size } = this.product.stock;
      const sizeList = size && size !== "" ? JSON.parse(size) : [];
      this.product.stock.size = sizeList.filter(item => item.content !== "No Stock.");
      this.size = this.product.stock.size.length ? this.product.stock.size[0]._attributes.Size : null;
    } catch (error) {
      this.product.stock.size = null;
    }
  }

  private assertStockExists() {
    if (!this.product.stock && !this.product.stock.size && !this.product.stock.inStock) {
      throw new Error('Stock doesnt exist.');
    }
  }

  isInStock(): boolean {
    return this.product.stock && this.product.stock.inStock;
  }

  productHasSizes(): boolean {
    return this.product.stock && this.product.stock.size && this.product.stock.size.length;
  }
  
  starIsActive(level): boolean {
    return this.averageRanking >= Number(level);
  }
  setSize(size) {
    this.size = size;
  }
}
