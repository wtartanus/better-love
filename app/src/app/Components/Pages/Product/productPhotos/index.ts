import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'product-photos',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class ProductPhotos implements OnInit {
  @Input() product: any;
  photos: Array<string> = new Array();

  ngOnInit() {
    this.setPhotos();
  }

  setPhotos() {
    const { ximage, ximage2, ximage3, ximage4, ximage5 } = this.product;
    const mainImage = `https://s3.eu-west-2.amazonaws.com/petitcheri/products/${ximage2 || ximage}`;

    let photos = [ximage3, ximage4, ximage5].filter(img => img && img !== '{}');
    photos = photos.map(img => `https://s3.eu-west-2.amazonaws.com/petitcheri/products/${img}`);

    this.photos = [mainImage, ...photos];
  }

  changePhoto(photo) {
    this.photos = [
      photo,
      ...this.photos.filter((img: any) => img !== photo)
    ];
    console.log(this.photos);
  }
}
