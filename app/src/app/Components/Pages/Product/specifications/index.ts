import { Component, Input } from '@angular/core';


@Component({
  selector: 'product-specifications',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class ProductSpecifications {
  @Input() product: any;
}