import { Component, Input, OnInit } from '@angular/core';
import { ReviewsService } from '../../../../Services';

@Component({
  selector: 'product-reviews',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class ProductReviews implements OnInit {
  @Input() private productId: string;
  reviews: Array<any> = new Array();
  reviewsShowCount: number = 3;
  paginationPages: Array<any> = new Array();
  paginationCurrentPage: Array<any> = new Array();
  paginationCurrentPageIndex: number = 0;
  paginationNav: Array<number> = new Array();

  constructor(private reviewsService: ReviewsService) {}

  async ngOnInit() {
    this.reviews = await this.reviewsService.getReview(this.productId);
    this.createReviewPagination();
    this.createPaginationNav();
  }

  createReviewPagination() {
    let items = [];
    this.reviews.forEach((review: any) => {
      items.push(review);
      if (items.length == this.reviewsShowCount) {
        this.paginationPages = [...this.paginationPages, items];
        items = [];
      }
    });
    if (items.length) this.paginationPages = [...this.paginationPages, items];
    this.paginationCurrentPage = this.paginationPages[0];
  }

  changePage(pageNumber: number) {
    this.paginationCurrentPage = this.paginationPages[pageNumber];
    this.paginationCurrentPageIndex = pageNumber;
    this.createPaginationNav();
    this.scrollToTheTop();
  }

  createPaginationNav() {
    this.paginationNav.length = 0;
    if (this.paginationPages.length <= 4) {
      this.paginationNav = this.paginationPages.map((item, index) => index);
    } else {
      let start = this.paginationCurrentPageIndex <= 4 ? 0 : this.paginationCurrentPageIndex;
      let end = this.paginationCurrentPageIndex + 4;
      end = end <= this.paginationPages.length ? end : this.paginationPages.length;

      for (; start <= end; start++) {
        this.paginationNav.push(start);
      }
    }
  }

  scrollToTheTop() {
    document.getElementsByClassName('reviews')[0].scrollTo(0, 0);
  }

  productHasReviews(): boolean {
    return Boolean(this.reviews) && Boolean(this.reviews.length);
  }

  starIsActive(level, score): boolean {
    return score >= Number(level);
  }

  isLastReview(index: number): boolean {
    return index == this.reviews.length - 1;
  }
}