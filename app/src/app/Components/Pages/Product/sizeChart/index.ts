import { Component } from '@angular/core';


@Component({
  selector: 'size-chart',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class SizeChart {}