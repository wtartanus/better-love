import { Component, Input } from '@angular/core';

@Component({
  selector: 'ng-product-listing',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class ProductListingComponent {
  @Input() product;

  getImageSrc(product: any): string {
    const { image, xlImage, xlImage2, xlImage3 } = product;
    if (this.isSrcProvided(image)) return `https://s3.eu-west-2.amazonaws.com/petitcheri/products/${image}`;
    if (this.isSrcProvided(xlImage)) return `https://s3.eu-west-2.amazonaws.com/petitcheri/products/${xlImage}`;
    if (this.isSrcProvided(xlImage2)) return `https://s3.eu-west-2.amazonaws.com/petitcheri/products/${xlImage2}`;
    if (this.isSrcProvided(xlImage3)) return `https://s3.eu-west-2.amazonaws.com/petitcheri/products/${xlImage3}`;
  }

  isSrcProvided(src: any): boolean {
    return src && src !== '' && src !== {};
  }

  isInStock(): boolean {
    return this.product.stock && this.product.stock.inStock;
  }
}
