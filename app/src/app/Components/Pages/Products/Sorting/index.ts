import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SortTypes } from '../config';

@Component({
  selector: 'ng-sorting',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class SortingComponent {
  @Input() products: Array<any>;
  @Output() itemsSorted = new EventEmitter();
  private selectedSort: SortTypes;

  sortBy(type: SortTypes) {
    this.activateSort(type);
    switch (type) {
      case SortTypes.LOW_TO_HIGH:
        this.itemsSorted.emit(this.sortLowToHigh(this.products));
        break;
      case SortTypes.HIGH_TO_LOW:
        this.itemsSorted.emit(this.sortHighToLow(this.products))
        break;
      case SortTypes.RANKING:
        this.itemsSorted.emit(this.sortByRanking(this.products))
        break;
    }
  }

  sortLowToHigh(list: Array<any>): Array<any> {
    return list.sort((a, b) => a.price - b.price);
  }

  sortHighToLow(list: Array<any>): Array<any> {
    return list.sort((a, b) => b.price - a.price);
  }

  sortByRanking(list: Array<any>): Array<any> {
    return list.sort((a, b) => a.price - b.price);
      // const value1 = this.productsService.reviewsByProductId[a.id];
      // const value2 = this.productsService.reviewsByProductId[b.id];
      // if ((!value1 && !value2) || (value1 === value2)) return 0;
      // if ((value1 && !value2) || (value1 > value2)) return -1;
      // if ((!value1 && value2) || (value1 < value2)) return 1;
      // return 0;
  }
  
  activateSort(type: SortTypes) {
    this.selectedSort = type;
  }
  isSortActive(type: SortTypes): boolean {
    return this.selectedSort === type;
  }
}

