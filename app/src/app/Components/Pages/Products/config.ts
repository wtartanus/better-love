export enum SortTypes {
  LOW_TO_HIGH,
  HIGH_TO_LOW,
  RANKING
}
  
export const THROTTLE = 300;
export const SCROLL_DISTANCE = 1;
export const SCROLL_UP_DISTANCE = 2;
export const PRODUCTS_DISPLAY_COUNT = 12;