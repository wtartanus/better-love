import { Component, Input, Output, EventEmitter } from '@angular/core';

import { THROTTLE, SCROLL_DISTANCE, SCROLL_UP_DISTANCE } from '../config';

@Component({
  selector: 'ng-products-listing',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class ProductsListingComponent {
  @Input() products;
  @Output() loadMoreProducts = new EventEmitter();
  loading: boolean;
  throttle = THROTTLE;
  scrollDistance = SCROLL_DISTANCE;
  scrollUpDistance = SCROLL_UP_DISTANCE;
  isNoResult(): boolean {
    return !this.products || !this.products.length;
  }

  onScrollDown() {
    this.loadMoreProducts.emit();
  }
}
