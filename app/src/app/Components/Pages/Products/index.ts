import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { BasketService, UtilitiesService, ProductsService, BreadcrumbsService, MessageService } from '../../../Services';

import { PRODUCTS_DISPLAY_COUNT } from './config';

@Component({
  selector: 'products',
  templateUrl: './layout.html'
})
export class ProductsComponent implements OnInit {
  heading: string;
  products = new Array();
  loading: boolean;
  loadingMoreProducts = false;
  private isSearch = false;
  private productsDisplayCount = PRODUCTS_DISPLAY_COUNT;
  private productsFetchStartIndex = 0;

  constructor(
    private message: MessageService,
    public productsService: ProductsService,
    public basket: BasketService,
    private route: ActivatedRoute,
    private router: Router,
    private utilities: UtilitiesService,
    private breadcrumbs: BreadcrumbsService
  ) {}

  ngOnInit() {
    this.utilities.scrollToTop();
    this.initialize();
  }

  initialize() {
    this.route.params
      .subscribe(async () => {
        this.setPageInLoadingMode();
        const category = this.route.snapshot.paramMap.get('category');
        const searchQuery = this.route.snapshot.paramMap.get('searchQuery');
        if (category) {
          this.breadcrumbs.setCurrentPosition(this.breadcrumbs.types.CATEGORY, category);
          const { data } = await this.productsService.getProductsByCategory(category, this.productsFetchStartIndex);
          this.products = data;
        } else if (searchQuery) {
          this.isSearch = true;
          const { data } = await this.productsService.getSearchProducts(searchQuery, this.productsFetchStartIndex);
          this.products = data;
        }
        this.updateProductsFetchStartIndex();
        this.setPageInLoadedMode();
        this.closeNav();
        this.heading = category || searchQuery;
    });
  }

  setPageInLoadingMode() {
    this.loading = true;
  }

  setPageInLoadedMode() {
    this.loading = false;
  }

  closeNav(): void {
    this.message.sendMessage('close-nav', {});
  }

  async loadMoreProdcuts(): Promise<any> {
    this.loadingMoreProducts = true;
    let products;
    if (this.isSearch) {
      const { data } = await this.productsService.getSearchProducts(this.heading, this.productsFetchStartIndex);
      products = data;
    } else {
      const { data } = await this.productsService.getProductsByCategory(this.heading, this.productsFetchStartIndex);
      products = data;
    }
    
    this.updateProductsFetchStartIndex();
    this.products = [...this.products, ...products];
    this.loadingMoreProducts = false;
  }

  updateProductsFetchStartIndex() {
    this.productsFetchStartIndex += this.productsDisplayCount;
  }

  updateWitSortedProducts(sortedProducts: any) {
    this.products = sortedProducts;
  }
}

// sudo scp -r -i ~/Documents/london.pem ~/Documents/dev/petit-cheri/app/dist ubuntu@3.9.108.165:/home/ubuntu/shop1/client/
