import { Component } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { MessageService } from '../../Services';

@Component({
  selector: 'home-component',
  templateUrl: './layout.html',
})
export class HomeComponent {
  private subscription: Subscription;
  presentSearchOverlay: boolean;

  constructor(private message: MessageService) {}

  closeNav(): void {
    this.message.sendMessage('close-nav', {});
  }

  displaySearchOverlay(event: any): void {
    this.presentSearchOverlay = true;
  }

  hideSearchOveraly(): void {
    this.presentSearchOverlay = false;
  }
}


