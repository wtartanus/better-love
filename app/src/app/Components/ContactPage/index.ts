import { Component, OnInit } from '@angular/core';
import { UtilitiesService } from '../../Services';

@Component({
  selector: 'Contact-Page',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class ContactPage implements OnInit {
  constructor(private utilities: UtilitiesService) {};

  ngOnInit() {
    this.utilities.scrollToTop();
  }
}
