import { Component } from '@angular/core';

@Component({
  selector: 'ng-loading',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class LoadingComponent {}