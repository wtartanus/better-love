import { Component } from '@angular/core';

@Component({
  selector: 'ng-inline-loading',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class InlineLoadingComponent {}