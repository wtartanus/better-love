import { Component, OnInit } from '@angular/core';
import { UtilitiesService } from '../../Services';

@Component({
  selector: 'Returns',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class Returns implements OnInit {
  constructor(private utilities: UtilitiesService) {};

  ngOnInit() {
    this.utilities.scrollToTop();
  }
}
