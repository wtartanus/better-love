import { Component, OnInit } from '@angular/core';
import { UtilitiesService } from '../../Services';
import { HOME_PAGE_PRODUCTS_DISPLAY_TYPES } from '../../Types';

@Component({
  selector: 'splash',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class SplashComponent implements OnInit {
  homePageProductsTypes = HOME_PAGE_PRODUCTS_DISPLAY_TYPES;
  showLogo: boolean = true;
  constructor(private utilities: UtilitiesService) {};
  ngOnInit() {
    this.utilities.scrollToTop();
  }
}
