import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { UtilitiesService } from '../../Services';

@Component({
  selector: 'TermsAndConditions',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class TermsAndConditions implements OnInit {
  constructor(private utilities: UtilitiesService) {};

  ngOnInit() {
    this.utilities.scrollToTop();
  }
}
