import { Component, OnInit } from '@angular/core';
import { UtilitiesService } from '../../Services';

@Component({
  selector: 'Privacy',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class Privacy implements OnInit {
  constructor(private utilities: UtilitiesService) {};

  ngOnInit() {
    this.utilities.scrollToTop();
  }
}
