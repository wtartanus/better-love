import { Component, OnInit } from '@angular/core';
import { UtilitiesService } from '../../Services';

@Component({
  selector: 'Delivery',
  templateUrl: './layout.html',
  styleUrls: ['./styles.css']
})
export class Delivery implements OnInit {
  constructor(private utilities: UtilitiesService) {};

  ngOnInit() {
    this.utilities.scrollToTop();
  }
}
