import {Injectable} from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { UtilitiesService } from "../utilities";

@Injectable()
export class Requestor {
  private http: Http;
  private utilities: UtilitiesService;
  private baseUrl: string;
  constructor(http: Http, utilties: UtilitiesService) {
    this.http = http;
    this.utilities = utilties;
    this.baseUrl = this.utilities.getBaseUrl();
  }

  get(url: string): Promise<any> {
    return this.http.get(this.baseUrl + url)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  post(url: string, msg: any): Promise<any> {
    return this.http.post(this.baseUrl + url, msg)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Something went wrong', error);
    return Promise.reject(error.message || error);
  };
}