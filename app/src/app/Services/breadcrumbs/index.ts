import { Injectable } from '@angular/core';
import { CategoriesService } from '../categories';
import { MessageService } from '../message';
import { BREADCRUMBS_UPDATED } from '../../Config/index';

enum types { CATEGORY, PRODUCT };

@Injectable()
export class BreadcrumbsService {
  crumbs: Array<any> = new Array();
  categories: CategoriesService;

  types = types;

  constructor(categories: CategoriesService, private message: MessageService) {
    this.categories = categories;
    this.message = message;
  }

  setCurrentPosition(type: types, name: string, productCategory?: string) {
    switch (type) {
      case this.types.CATEGORY:
        this.processCategoryBreadcrumbs(name);
        break;
      case this.types.PRODUCT:
        this.processProductBreadcrumbs(name, productCategory);
        break;
      default:
        console.error('Provided incorect type.');
    }
  }

  private async processCategoryBreadcrumbs(category: string) {
    this.crumbs = await this.findParents(category);
    this.message.sendMessage(BREADCRUMBS_UPDATED, this.crumbs);
  }

  private async processProductBreadcrumbs(name: string, category: string) {
    const crumbs = await this.findParents(category);
    this.crumbs = [...crumbs, name];
    this.message.sendMessage(BREADCRUMBS_UPDATED, this.crumbs);
  }

  private async findParents(category: string) {
    const tree = await this.categories.getCategoriesTree();
    const path = new Array<string>();
    this.findPath(path, category, tree);
    return path.reverse();
  }

  private findPath(path: string[], targetCategory: string, root?: any) {
    if (!root) return false;
    if (root.name === targetCategory) return true;
    for (let i = 0; i < root.children.length; i++) {
      if (this.findPath(path, targetCategory, root.children[i])) {
        path.push(root.children[i].name);
        return true;
      }
    }
    return false;
  }

  resetBreadcrumbs() {
    this.crumbs.length = 0;
  }
}
