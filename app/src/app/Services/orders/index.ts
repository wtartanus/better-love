import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Requestor } from '../requestor';

@Injectable()
export class OrdersService {
  private requestor: Requestor;

  constructor(requestor: Requestor) {
    this.requestor = requestor;
  };

  requestOrder(order: any) {
    this.requestor.post('order', order);
  }

}
