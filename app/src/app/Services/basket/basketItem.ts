export interface BasketItem {
  quantity: number;
  product: any;
  cost: number;
  size: any;
};