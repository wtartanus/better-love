import { Injectable } from '@angular/core';
import { MessageService } from '../message';
import { BasketItem } from './basketItem';
import { TOAST_TYPES } from '../../Types/index';
import { TOAST_DISPLAY_MESSAGE } from '../../Config/index';

@Injectable()
export class BasketService {
  public totalCost: number = 0;
  public basketItems: Array<BasketItem> = new Array();
  public basketItemsById: { item?: BasketItem } = {};
  public itemsCount: number = 0;

  constructor(private message: MessageService) { }

  addToBasket(quantity: number, product: any, size: any) {
    try {
      this.assertThatQuantityAndProductAreProvided(quantity, product);

      if (this.isProductInBasket(product.id)) {
        this.updatedExistingProduct(quantity, product, size);
      } else {
        this.addNewProduct(quantity, product, size);
      }

      this.totalCost = parseFloat((this.totalCost + (product.price * quantity)).toFixed(2));
      this.itemsCount += quantity;
      this.informAboutBasketUpdate();
      this.sendToastAboutItemsAdded(product.name);
    } catch (error) {
      console.error('Values not provided');
    }
  }

  private assertThatQuantityAndProductAreProvided(quantity, product) {
    if (!quantity && !product) throw new Error();
  }

  private isProductInBasket(productId) {
    return Boolean(this.basketItemsById[productId]);
  }

  private updatedExistingProduct(quantity, product, size) {
    const { id: productId } = product;
    this.basketItemsById[productId].cost += product.price * quantity;
    this.basketItemsById[productId].quantity += quantity;

    if (this.basketItemsById[productId].size[size]) {
      this.basketItemsById[productId].size[size].quantity += quantity;
    } else {
      this.basketItemsById[productId].size[size] = { value: size, quantity };
    }
    
    this.basketItems = this.basketItems.filter(item => item.product.id !== productId);
    this.basketItems.push({...this.basketItemsById[productId]});
  }

  private addNewProduct(quantity, product, size) {
    const basketItem = {
        quantity: quantity,
        product: product,
        cost: product.price * quantity,
        size: {}
    };

    if (size) basketItem.size = { value: size, quantity };

    this.basketItems.push(basketItem);
    this.basketItemsById[product.id] = basketItem;
  }

  private sendToastAboutItemsAdded(name) {
    const toastMessage = {
      type: TOAST_TYPES.success,
      title: name,
      message: 'Was Added To Cart'
    }
    this.message.sendMessage(TOAST_DISPLAY_MESSAGE, toastMessage);
  }

  removeFromBasket(product: any): void{
    try {
      this.assertThatProductExist(product);
      
      this.decreaseBasketValues(product.id);
      this.removeProductFromBasketCollections(product.id);
      this.informAboutBasketUpdate();
    } catch (error) {
      console.error('product not provided');
    }
  }

  private assertThatProductExist(product: any) {
    if (!product || !this.basketItemsById[product.id]) throw new Error();
  }
  
  private decreaseBasketValues(productId) {
    this.totalCost -= this.basketItemsById[productId].cost;
    this.itemsCount -= this.basketItemsById[productId].quantity;
  }

  private removeProductFromBasketCollections(productId) {
    this.removeProductFromItemsById(productId);
    this.removeProductFromBasketItems(productId);
  }

  private removeProductFromItemsById(productId) {
    this.basketItemsById[productId] = undefined;
  }

  private removeProductFromBasketItems(productId) {
    this.basketItems = this.basketItems.filter(item => item.product.id !== productId);
  }

  resetBasket() {
    this.totalCost = 0;
    this.basketItems.length = 0;
    this.basketItemsById = {};
    this.itemsCount = 0;

    this.informAboutBasketUpdate();
  }

  private informAboutBasketUpdate() {
    const { totalCost, itemsCount } = this;
    this.message.sendMessage('basket-update', { totalCost, itemsCount });
  }
}
