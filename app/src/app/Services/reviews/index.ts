import { Injectable } from '@angular/core';
import { Requestor } from '../requestor';

@Injectable()
export class ReviewsService {
  private requestor: Requestor;
  private reviewsByProductId: any;

  constructor(requestor: Requestor) {
    this.requestor = requestor;
  };

  getReview(id: string): Promise<any> | any {
    return new Promise(async (resolve) => {
      if (this.reviewsByProductId[id]) {
        resolve([...this.reviewsByProductId[id]]);
      } else {
        await this.fetchReviews(id);
        resolve([...this.reviewsByProductId[id]]);
      }
    });
  }


  private fetchReviews(id): Promise<any> {
    return this.requestor.get('reviews/' + id)
      .then((result) => {
        const { data } = result;
        this.reviewsByProductId[id] = data;
    });
  }
}
