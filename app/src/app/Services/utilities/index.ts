import { Injectable, isDevMode } from '@angular/core';

@Injectable()
export class UtilitiesService {
  scrollToTop() {
    try{
      this.assertWindowExist();
      window.scrollTo(0, 0);
    } catch (error) {}
  }

  private assertWindowExist(): any {
    if (typeof window === 'undefined') throw new Error();
  }

  getBaseUrl(): string {
    return isDevMode() ? 'http://localhost:8080/' : '/api';
  }
}
