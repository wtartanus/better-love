import { Injectable } from '@angular/core';
import { Requestor } from '../requestor';
import { ICategory } from '../../Interfaces';

@Injectable()
export class CategoriesService {
  private requestor: Requestor;
  private categoriesTree: any;
  private categoriesFlat: any;
  private categories: ICategory[];

  private categoriesByName: any;

  constructor(requestor: Requestor) {
    this.requestor = requestor;
  };

  getCategories(): Promise<any> | ICategory[] {
    return new Promise(async (resolve) => {
      if (this.categories) {
        resolve([...this.categories]);
      } else {
        await this.fetchCategories();
        resolve([...this.categories]);
      }
    });
  }

  private fetchCategories(): Promise<any> {
    return this.requestor.get('categories')
      .then((result) => {
        const { data } = result;
        this.categories = data;
        this.initCategoriesByName(this.categories);
    });
  }

  private initCategoriesByName(categories: ICategory[]) {
    const categoriesByName = {};
    categories.forEach((category: ICategory) => {
      categoriesByName[category.name] = category;
    });
    this.categoriesByName = categoriesByName;
  }

  getCategoriesTree(): Promise<any> {
    return new Promise(async (resolve) => {
      if (this.categoriesTree) {
        resolve({...this.categoriesTree});
      } else {
        await this.fetchCategoriesTree();
        resolve({...this.categoriesTree});
      }
    });
  }

  private fetchCategoriesTree(): Promise<any> {
    return this.requestor.get('categoryTree')
      .then((result) => {
        const { categoriesTree } = result.data;
        this.categoriesTree = categoriesTree;
      });
  }


  getFlatCategories(): Promise<any>  {
    return new Promise(async (resolve) => {
      if (this.categoriesFlat) {
        resolve({...this.categoriesFlat});
      } else {
        await this.fetchFlatCategories();
        resolve({...this.categoriesFlat});
      }
    });
  }

  private fetchFlatCategories() {
    return this.requestor.get('categoryFlat')
      .then((result) => {
        const { flatCategories } = result.data;
        this.categoriesFlat = flatCategories;
      });
  }

  getCategoriesByName() {
    if (this.categoriesByName) {
      return {...this.categoriesByName};
    }
  }
}
