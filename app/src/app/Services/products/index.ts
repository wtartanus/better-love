import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Requestor } from '../requestor';
import { MessageService } from "../message";
import { IProduct } from '../../Interfaces';

@Injectable()
export class ProductsService {
  private requestor: Requestor;
  private scriptLoaded: boolean = false;

  constructor(requestor: Requestor, private message: MessageService) {
    this.requestor = requestor;
    this.message = message;  
  };

  getProductsByCategory(category: string, startIndex: number): Promise<any> {
    return this.fetchCategory(category, startIndex);
  }

  fetchCategory(category: string, startIndex): Promise<any> {
    return this.requestor.get(`category/${category}?from=${startIndex}`)
  }

  getSearchProducts(searchQuery: string, startIndex: number): Promise<any>{
    return this.fetchSearch(searchQuery, startIndex);
  }

  fetchSearch(searchQuery: string, startIndex): Promise<any> {
    return this.requestor.get(`search/${searchQuery}?from=${startIndex}`);
  }

  getProductByName(name: string): Promise<any> {
    return this.requestor.get(`product/${name}`);
  }

  getFavourites(): Promise<any> {
    return this.requestor.get('favourites');
  }

  getDiscounted(): Promise<any> {
    return this.requestor.get('discounted');
  }

  getScriptLoaded(): boolean {
    return this.scriptLoaded;
  }

  setScriptLoaded(loaded: boolean): void {
    this.scriptLoaded = loaded;
  }

  getReviews(id: string): Promise<any> {
    return this.requestor.get('reviews' + id);
  };

  addReview(review: any): Promise<any> {
    return this.requestor.post('review', review);
  }
}
