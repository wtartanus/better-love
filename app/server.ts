// These are important and needed before anything else
import 'zone.js/dist/zone-node';
import 'reflect-metadata';

import { enableProdMode } from '@angular/core';

import * as express from 'express';
import * as compression from 'compression';
import { join } from 'path';
import * as bluebird from "bluebird";
import * as mongoose from "mongoose";
import * as dotenv from "dotenv";
import * as fs from 'fs';
dotenv.config({ path: ".env.example" });

import apiRouter from './server/routes';

import { InventoryService, setDataUpdate } from "./server/services";

import { MONGODB_URI } from "./server/util/secrets";

// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

const mongoUrl = MONGODB_URI;
(<any>mongoose).Promise = bluebird;
mongoose.connect(mongoUrl, {useMongoClient: true}).then(
  () => { /** ready to use. The `mongoose.connect()` promise resolves to undefined. */ },
).catch(err => {
  console.log("MongoDB connection error. Please make sure MongoDB is running. " + err);
  // process.exit();
});

// Express server
const app = express();

const PORT = process.env.PORT || 80;
const DIST_FOLDER = process.cwd();

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const { AppServerModuleNgFactory, LAZY_MODULE_MAP } = require('./dist/server/main');

// Express Engine
import { ngExpressEngine } from '@nguniversal/express-engine';
// Import module map for lazy loading
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';


app.engine('html', ngExpressEngine({
  bootstrap: AppServerModuleNgFactory,
  providers: [
    provideModuleMap(LAZY_MODULE_MAP)
  ]
}));

app.set('view engine', 'html');
app.set('views', join(DIST_FOLDER, 'browser'));

// TODO: implement data requests securely
// app.get('/api/*', (req, res) => {
//   res.status(404).send('data requests are not supported');
// });
app.use(compression());
app.use('/api', apiRouter);

app.get('/sitemap', (req, res) => {
  res.set('Content-Type', 'text/xml');
  res.send(fs.readFileSync('./server/sitemap.xml', {encoding: 'utf-8'}))
});

// Server static files from /browser
app.get('*.*', express.static(join(DIST_FOLDER, 'browser')));

// All regular routes use the Universal engine
app.get('*', (req, res) => {
  res.render('index', { req });
});

setDataUpdate(InventoryService.updateInventory);

// Start up the Node server
app.listen(PORT, () => {
  console.log(`Node server listening on http://localhost:${PORT}`);
});
